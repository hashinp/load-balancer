Feature:
  In order to perform my function
  As a task
  I want to be run by a worker

  Scenario: Worker executes a task
    Given task
    And worker
    When worker executes task
    Then the task is processed