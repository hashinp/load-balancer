<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Cleoo\LoadBalancer;
use Cleoo\LowestLoadAlgorithm;
use Cleoo\RoundRobinAlgorithm;
use Cleoo\Task;
use Cleoo\Worker;
use PHPUnit\Framework\Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * @var Worker
     */
    private $worker;
    /**
     * @var Task
     */
    private $task;
    /**
     * @var array
     */
    private $workers = [];
    private $tasks = [];
    /**
     * @var LoadBalancer
     */
    private $loadBalancer;
    private $exception;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given task
     */
    public function task()
    {
        $this->task = new Task();
    }

    /**
     * @Given worker
     */
    public function worker()
    {
        $this->worker = new Worker();
    }

    /**
     * @When worker executes task
     */
    public function workerExecutesTask()
    {
        $this->worker->processTask($this->task);
    }

    /**
     * @Then the task is processed
     */
    public function theTaskIsProcessed()
    {
        Assert::assertTrue($this->task->isPerformed(), 'The task was not processed');
    }

    /**
     * @Given Load Balancer
     */
    public function loadBalancer()
    {
        $this->loadBalancer = new LoadBalancer(array_values($this->workers), new RoundRobinAlgorithm());
    }

    /**
     * @Given worker :id
     */
    public function worker2(string $id)
    {
        $this->workers[$id] = new Worker();
    }

    /**
     * @Given task :id
     */
    public function task2(string $id)
    {
        $this->tasks[$id] = new Task();
    }

    /**
     * @When task :arg1 is sent to the load balancer
     */
    public function taskIsSentToTheLoadBalancer(string $id)
    {
        try {
            $this->loadBalancer->processTask($this->tasks[$id]);
        } catch (Exception $exception) {
            $this->exception = $exception;
        }
    }

    /**
     * @Then worker :arg1 is selected for processing the task :arg2
     */
    public function workerIsSelectedForProcessingTheTask(string $workerId, string $taskId)
    {
        Assert::assertTrue($this->workers[$workerId]->hasProcessedTask($this->tasks[$taskId]));
    }

    /**
     * @Given worker :arg1 is busy
     */
    public function workerIsBusy(string $workerId)
    {
        $this->workers[$workerId]->beBusy();
    }

    /**
     * @Given worker :arg1 load is :arg2
     */
    public function workerLoadIs(string $workerId, int $load)
    {
        $this->workers[$workerId]->setLoad($load);
    }

    /**
     * @Given Load Balancer with Lowest Load Algorithm
     */
    public function loadBalancerWithLowestLoadAlgorithm()
    {
        $this->loadBalancer = new LoadBalancer(array_values($this->workers), new LowestLoadAlgorithm());
    }

    /**
     * @Then error occurs :arg1
     */
    public function errorOccurs($message)
    {
        Assert::assertEquals($this->exception->getMessage(), $message, 'Error did not occur');
    }

    /**
     * @Given load balancer with no workers and Lowest Load Algorithm
     */
    public function loadBalancerWithNoWorkersAndLowestLoadAlgorithm()
    {
        $this->loadBalancer = new LoadBalancer([], new LowestLoadAlgorithm());
    }
}
