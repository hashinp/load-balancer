<?php

namespace Cleoo;

class Task implements TaskInterface
{
    private $performed = false;

    public function perform()
    {
        $this->performed = true;
    }

    public function isPerformed(): bool
    {
        return $this->performed;
    }
}