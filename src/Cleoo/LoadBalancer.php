<?php

namespace Cleoo;

use RuntimeException;

class LoadBalancer
{
    /**
     * @var array
     */
    private $workers;
    /**
     * @var RoundRobinAlgorithm
     */
    private $algorithm;

    public function __construct(array $workers, BalancingAlgorithmInterface $algorithm)
    {
        $this->workers = $workers;
        $this->algorithm = $algorithm;
    }

    public function processTask(Task $task)
    {
        if (empty($this->workers)) {
            throw new RuntimeException('No workers provided');
        }
        $worker = $this->algorithm->selectWorker($this->workers);
        $worker->processTask($task);
    }
}
