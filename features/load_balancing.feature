Feature: In order to successfully distribute workload
  As a system
  I need to load balance the tasks to the  workers based on a round robin, or lowest load algorithm

  Scenario: Round robin
    Given worker "W1"
    And worker "W2"
    And Load Balancer
    And task "T1"
    And task "T2"
    And task "T3"
    When task "T1" is sent to the load balancer
    Then worker "W1" is selected for processing the task "T1"
    When task "T2" is sent to the load balancer
    Then worker "W2" is selected for processing the task "T2"
    When task "T3" is sent to the load balancer
    Then worker "W1" is selected for processing the task "T3"

  Scenario: Round robin - Busy Worker
    Given worker "W1"
    And worker "W2"
    And Load Balancer
    And task "T1"
    And worker "W1" is busy
    When task "T1" is sent to the load balancer
    Then worker "W2" is selected for processing the task "T1"

  Scenario: No Workers
    Given load balancer with no workers and Lowest Load Algorithm
    And task "T1"
    When task "T1" is sent to the load balancer
    Then error occurs "No workers provided"

  Scenario: Round robin - All workers busy
    Given worker "W1"
    And worker "W2"
    And Load Balancer
    And task "T1"
    And worker "W1" is busy
    And worker "W2" is busy
    When task "T1" is sent to the load balancer
    Then error occurs "All workers are busy"


  Scenario: Lowest Load Worker Selection
    Given worker "W1"
    And worker "W1" load is "2"
    And worker "W2"
    And worker "W2" load is "1"
    And Load Balancer with Lowest Load Algorithm
    And task "T1"
    And task "T2"
    When task "T1" is sent to the load balancer
    Then worker "W2" is selected for processing the task "T1"



