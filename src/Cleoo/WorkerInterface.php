<?php

namespace Cleoo;

interface WorkerInterface
{
    public function processTask(TaskInterface $task);

    public function isBusy(): bool;

    public function getLoad(): int;
}