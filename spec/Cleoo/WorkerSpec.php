<?php

namespace spec\Cleoo;

use Cleoo\Task;
use Cleoo\TaskInterface;
use Cleoo\Worker;
use PhpSpec\ObjectBehavior;

/**
 * Class WorkerSpec
 * @package spec\Cleoo
 * @mixin Worker
 */
class WorkerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Worker::class);
    }

    function it_processes_task(TaskInterface $task)
    {
        $task->perform()->shouldBeCalled();
        $this->processTask($task);
        $this->shouldHaveProcessedTask($task);
    }

    function it_is_busy()
    {
        $this->beBusy();
        $this->shouldBeBusy();
    }

    function it_has_load()
    {
        $this->setLoad(2);
        $this->getLoad()->shouldBe(2);
    }
}
