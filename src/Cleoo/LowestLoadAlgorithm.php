<?php

namespace Cleoo;


class LowestLoadAlgorithm implements BalancingAlgorithmInterface
{
    public function selectWorker(array $workers): WorkerInterface
    {
        $load = $workers[0]->getLoad();
        $minLoadWorkerIndex = 0;

        $nonBusyWorkerFound = false;

        foreach($workers as $index => $worker) {

            if (!$worker->isBusy() && !$nonBusyWorkerFound) {
                $nonBusyWorkerFound = true;
                $minLoadWorkerIndex = $index;
                $load = $worker->getLoad();
            }

            if ($worker->getLoad() < $load && !$worker->isBusy()) {
                $minLoadWorkerIndex = $index;
                $load = $worker->getLoad();
            }
        }

        if (!$workers[$minLoadWorkerIndex]->isBusy()) {
            return $workers[$minLoadWorkerIndex];
        }

        throw new AllWorkersBusyException('All workers are busy');
    }
}
