<?php

namespace spec\Cleoo;

use Cleoo\RoundRobinAlgorithm;
use Cleoo\Worker;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class RoundRobinAlgorithm
 * @package spec\Cleoo
 * @mixin RoundRobinAlgorithm
 */
class RoundRobinAlgorithmSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(RoundRobinAlgorithm::class);
    }

    function it_selects_worker(Worker $w1, Worker $w2)
    {
        $w1->isBusy()->shouldBeCalled()->willReturn(false);
        $w2->isBusy()->shouldBeCalled()->willReturn(false);
        $this->selectWorker([$w1, $w2])->shouldBe($w1);
        $this->selectWorker([$w1, $w2])->shouldBe($w2);

        $w1->isBusy()->shouldBeCalled()->willReturn(true);
        $this->selectWorker([$w1, $w2])->shouldBe($w2);

        $w1->isBusy()->shouldBeCalled()->willReturn(false);
        $this->selectWorker([$w1, $w2])->shouldBe($w1);
        $this->selectWorker([$w1, $w2])->shouldBe($w2);
    }
}
