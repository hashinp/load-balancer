Running
=======
```
composer install
composer test
```

spec directory contains PHPSpec specifications (Unit testing/object design)
features directory contains behavioural tests (Domain testing)