<?php

namespace spec\Cleoo;

use Cleoo\LowestLoadAlgorithm;
use Cleoo\Worker;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class LowestLoadAlgorithmSpec
 * @package spec\Cleoo
 * @mixin LowestLoadAlgorithm
 */
class LowestLoadAlgorithmSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(LowestLoadAlgorithm::class);
    }

    function it_selects_worker(Worker $w1, Worker $w2, Worker $w3)
    {
        $w1->isBusy()->willReturn(false);
        $w2->isBusy()->willReturn(false);
        $w3->isBusy()->willReturn(false);

        $w1->getLoad()->willReturn(2);
        $w2->getLoad()->willReturn(1);

        $this->selectWorker([$w1, $w2])->shouldBe($w2);

        $w1->getLoad()->willReturn(2);
        $w2->getLoad()->willReturn(1);

        $this->selectWorker([$w1, $w2])->shouldBe($w2);

        $w1->getLoad()->willReturn(1);
        $w2->getLoad()->willReturn(2);

        $this->selectWorker([$w1, $w2])->shouldBe($w1);

        $w1->getLoad()->willReturn(3);
        $w2->getLoad()->willReturn(1);
        $w3->getLoad()->willReturn(2);
        $w2->isBusy()->shouldBeCalled()->willReturn(true);

        $this->selectWorker([$w1, $w2, $w3])->shouldBe($w3);

        $w1->isBusy()->willReturn(true);
        $w1->getLoad()->willReturn(1);
        $w2->isBusy()->willReturn(false);
        $w2->getLoad()->willReturn(2);
        $w3->isBusy()->willReturn(true);
        $w3->getLoad()->willReturn(3);
        $this->selectWorker([$w1, $w2, $w3])->shouldBe($w2);

        $w1->isBusy()->willReturn(true);
        $w1->getLoad()->willReturn(1);
        $w2->isBusy()->willReturn(false);
        $w2->getLoad()->willReturn(3);
        $w3->isBusy()->willReturn(false);
        $w3->getLoad()->willReturn(1);
        $this->selectWorker([$w1, $w2, $w3])->shouldBe($w3);
    }
}
