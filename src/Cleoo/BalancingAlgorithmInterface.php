<?php

namespace Cleoo;

interface BalancingAlgorithmInterface
{
    public function selectWorker(array $workers): WorkerInterface;
}