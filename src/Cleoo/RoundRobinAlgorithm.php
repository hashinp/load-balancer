<?php

namespace Cleoo;

use InvalidArgumentException;

class RoundRobinAlgorithm implements BalancingAlgorithmInterface
{
    private $lastWorker = -1;

    public function selectWorker(array $workers): WorkerInterface
    {
        if (empty($workers)) {
            throw new InvalidArgumentException('No workers given');
        }

        $workersChecked = 0;

        while(true) {

            if ($this->lastWorker == count($workers) - 1) {
                $this->lastWorker = 0;
            } else {
                $this->lastWorker++;
            }

            $worker = $workers[$this->lastWorker];
            $workersChecked++;

            if (!$worker->isBusy()){
                return $worker;
            }

            if ($workersChecked == count($workers)) {
                break;
            }


        }

        throw new AllWorkersBusyException('All workers are busy');
    }
}
