<?php

namespace spec\Cleoo;

use Cleoo\RoundRobinAlgorithm;
use Cleoo\LoadBalancer;
use Cleoo\Task;
use Cleoo\Worker;
use PhpSpec\ObjectBehavior;

/**
 * Class LoadBalancerSpec
 * @package spec\Cleoo
 * @mixin LoadBalancer
 */
class LoadBalancerSpec extends ObjectBehavior
{
    function let(Worker $w1, Worker $w2, RoundRobinAlgorithm $algorithm)
    {
        $this->beConstructedWith([$w1, $w2],  $algorithm);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(LoadBalancer::class);
    }

    function it_processes_task(Task $t1, Worker $w1, Task $t2, Worker $w2, RoundRobinAlgorithm $algorithm)
    {
        $algorithm->selectWorker([$w1, $w2])->shouldBeCalled()->willReturn($w1);
        $w1->processTask($t1)->shouldBeCalled();
        $this->processTask($t1);

        $algorithm->selectWorker([$w1, $w2])->shouldBeCalled()->willReturn($w2);
        $w2->processTask($t2)->shouldBeCalled();
        $this->processTask($t2);
    }
}
