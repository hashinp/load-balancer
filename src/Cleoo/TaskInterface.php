<?php

namespace Cleoo;

interface TaskInterface
{
    public function perform();

    public function isPerformed(): bool;
}