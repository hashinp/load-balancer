<?php

namespace spec\Cleoo;

use Cleoo\Task;
use Cleoo\Worker;
use PhpSpec\ObjectBehavior;

/**
 * Class TaskSpec
 * @package spec\Cleoo
 * @mixin Task
 */
class TaskSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Task::class);
    }

    function it_is_performed(Worker $worker)
    {
        $this->perform($worker);
        $this->shouldBePerformed();
    }
}
