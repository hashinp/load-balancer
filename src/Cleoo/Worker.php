<?php

namespace Cleoo;

class Worker implements WorkerInterface
{
    private $busy = false;
    private $processedTasks = [];
    private $load;

    public function processTask(TaskInterface $task)
    {
        $this->busy = true;
        $task->perform();
        $this->processedTasks[] = spl_object_hash($task);
        $this->busy = false;
    }

    public function beBusy()
    {
        $this->busy = true;
    }

    public function isBusy(): bool
    {
        return $this->busy;
    }

    public function hasProcessedTask(TaskInterface $task): bool
    {
        return in_array(spl_object_hash($task), $this->processedTasks);
    }

    public function setLoad(int $load)
    {
        $this->load = $load;
    }

    public function getLoad(): int
    {
        return $this->load;
    }
}
